package org.projet.golf.rulesgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class RulesgeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(RulesgeneratorApplication.class, args);
	}

}
