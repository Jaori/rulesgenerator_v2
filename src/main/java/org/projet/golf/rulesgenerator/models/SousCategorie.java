package org.projet.golf.rulesgenerator.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class SousCategorie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer num;
    private String titre;
    @Column(length = 5000)
    private String objet;


    @ManyToOne
    @JoinColumn(name="FK_CAT_ID")
    private Categorie categorie;


    @OneToMany(mappedBy = "sousCategorie")
    private List<Regle> regles= new ArrayList<Regle>();

    public SousCategorie(){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public List<Regle> getRegles() {
        return regles;
    }

    public void setRegles(List<Regle> regles) {
        this.regles = regles;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }
}
