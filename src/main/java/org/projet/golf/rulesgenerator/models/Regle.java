package org.projet.golf.rulesgenerator.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Regle {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer num;
    private String titre;

    @Column(length= 20000)
    private String contenu;

    @ManyToOne
    private SousCategorie sousCategorie;

    public Regle(){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public SousCategorie getSousCategorie() {
        return sousCategorie;
    }

    public void setSousCategorie(SousCategorie sousCategorie) {
        this.sousCategorie = sousCategorie;
    }

    public String getCode() {
        return this.getSousCategorie().getCategorie().getCode()+'.'+this.getSousCategorie().getNum()+'.'+this.getNum();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Regle)) return false;
        Regle regle = (Regle) o;
        return Objects.equals(getId(), regle.getId()) &&
                Objects.equals(getNum(), regle.getNum()) &&
                Objects.equals(getTitre(), regle.getTitre()) &&
                Objects.equals(getContenu(), regle.getContenu());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNum(), getTitre(), getContenu());
    }
}
