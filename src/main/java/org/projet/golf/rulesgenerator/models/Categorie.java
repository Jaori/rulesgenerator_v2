package org.projet.golf.rulesgenerator.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Categorie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String titre;
    private String code;

    @OneToMany(mappedBy = "categorie")
    private List<SousCategorie> sousCategorieList= new ArrayList<SousCategorie>();

    public Categorie(){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<SousCategorie> getSousCategorieList() {
        return sousCategorieList;
    }

    public void setSousCategorieList(List<SousCategorie> sousCategorieList) {
        this.sousCategorieList = sousCategorieList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Categorie)) return false;
        Categorie categorie = (Categorie) o;
        return getId().equals(categorie.getId()) &&
                Objects.equals(getTitre(), categorie.getTitre()) &&
                Objects.equals(getCode(), categorie.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitre(), getCode());
    }

}
