package org.projet.golf.rulesgenerator.models;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SousCategorieRepository extends JpaRepository<SousCategorie,Long>{

}
