package org.projet.golf.rulesgenerator.controller;

import org.projet.golf.rulesgenerator.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class MenuController {

    @Autowired
    private CategorieRepository categorieRepository;

    @Autowired
    private SousCategorieRepository sousCategorieRepository;

    @Autowired
    private RegleRepository regleRepository;

    @RequestMapping("/")
    public String racine(){
        return "connexion";
    }

    @RequestMapping("/Acceuil")
    public String acceuil(){

        return "inscription";
    }

    @RequestMapping("/Regles")
    public String categories(Model model) {
        List<Categorie> categorie = categorieRepository.findAll();
        for (Categorie cat: categorie) {
            List<SousCategorie> sCats = cat.getSousCategorieList();
            for (SousCategorie sCat: sCats) {
            }
        }
        model.addAttribute("categories",categorie);
        return "rules";
        }

    @RequestMapping("/Modifier")
    public String lien1(){

        return "modifCat";
    }

    @RequestMapping("/Creer")
    public String lien2(){

        return "creer";
    }

    @RequestMapping("/Formulaire")
    public String lien3(){

        return "form";
    }

    @RequestMapping("/Connexion")
    public String connexion() {

        return "connexion";
    }

    @RequestMapping("/Inscription")
    public String inscription(){

        return "inscription";
    }

}
