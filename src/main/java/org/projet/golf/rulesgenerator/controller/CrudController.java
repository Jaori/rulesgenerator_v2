package org.projet.golf.rulesgenerator.controller;

import org.projet.golf.rulesgenerator.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CrudController {

    @Autowired
    private CategorieRepository categorieRepository;

    @Autowired
    private SousCategorieRepository sousCategorieRepository;

    @Autowired
    private RegleRepository regleRepository;


    /*  TODO algo qui oriente le créateur a choisir le bon "code" de "catégorie" (ect)
     *   le bon "num" de "sous catégorie" et un "num" de "regle"
     *   Les trois étant obligatoire ..
     * */

    @RequestMapping(value = "/Creation", method = RequestMethod.POST)
    public String creerRegle(Model model, @RequestParam("code") String c,
                             @RequestParam("titre") String t, @RequestParam("num") Integer n,
                             @RequestParam("titre2") String t2, @RequestParam("objet") String o,
                             @RequestParam("contenu") String co) {
        Categorie categorie = new Categorie();
        categorie.setCode(c);
        categorie.setTitre(t);
        categorieRepository.save(categorie);

        SousCategorie sousCategorie = new SousCategorie();
        sousCategorie.setNum(n);
        sousCategorie.setTitre(t2);
        sousCategorie.setObjet(o);
        sousCategorie.setCategorie(categorie);

        sousCategorieRepository.save(sousCategorie);

        Regle regle = new Regle();
        regle.setContenu(co);
        regle.setSousCategorie(sousCategorie);

        regleRepository.save(regle);

        return "redirect:Creer";
    }

    // modifCat formulaire

    @RequestMapping(value = "/Categorie/{id}", method = RequestMethod.GET)
    public String modifcategorie(Model model, @PathVariable("id") Long id) {
        Categorie categorie = categorieRepository.findById(id).get();
        System.out.println("id ="+categorie.getId());
        model.addAttribute("categorie", categorie);
        return "modifCat";
        // Pas de redirect sur un get
    }

    @RequestMapping(value = "/Categorie/{id}/Modif", method = RequestMethod.POST)
    public String modifierCat(Model model, @PathVariable("id") Long id, @RequestParam("titre") String t) {

        Categorie categorie = categorieRepository.findById(id).get();
        categorie.setTitre(t);
        categorieRepository.save(categorie);

        return "redirect:/Regles";

        // redirect pour eviter le renvoi du formulaire en bdd
    }
}
     //     TODO récupérer les identifiants de la rubrique à modifier
     //     TODO mettre en place le controller qui enverra les données de la rubrique de
     //     manière à remplacer les données quand il s'agira de faire une modification


