package org.projet.golf.rulesgenerator.controller;

import org.projet.golf.rulesgenerator.models.GroupRepository;
import org.projet.golf.rulesgenerator.models.Groups;
import org.projet.golf.rulesgenerator.models.User;
import org.projet.golf.rulesgenerator.models.UserRepository;
import org.projet.golf.rulesgenerator.service.JpaUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class InscController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupRepository groupRepository;

    @RequestMapping(value = "/Inscriptions", method = RequestMethod.POST)
    public String inscritUtilisateur(Model model, @RequestParam("name") String n0, @RequestParam("email") String m1,
                                     @RequestParam("psw") String m2, @RequestParam("psw-repeat") String m3) {

        JpaUserService insc = new JpaUserService();
        insc.setGroupDao(groupRepository);
        insc.setUserDao(userRepository);
        insc.setbCryptPasswordEncoder(new BCryptPasswordEncoder());
        User userX = new User();
        userX.setName(n0);
        userX.setMail(m1);
        userX.setPassword(m2);
        // user.setGroups(Groups, "lambda");       ?????????????????????????????????????

        // TODO prévoir d'atribuer un role a chaque utilisateur
        // a l'inscription qu'il soit "user lambda"
        // puis plustard l'admin peut attribuer d'autres rôles

        char i;
        // 1ere vérification : égalité des mots de passes
        if (m2.equals(m3)) {
            // Les vérifications suivante concerne la forme des caractères(Maj, Min, ou Digits)
            for (i = 0; i < m2.length(); i++) {
                int isUp = 0;
                int isLow = 0;
                int isDgt = 0;
                if (Character.isUpperCase(m2.charAt(i))) {
                    isUp++;
                } else
                    continue;
                for (i = 0; i < m2.length(); i++) {
                    if (Character.isLowerCase(m2.charAt(i))) {
                        isLow++;
                    } else
                        continue;
                    for (i = 0; i < m2.length(); i++) {
                        if (Character.isDigit(m2.charAt(i))) {
                            isDgt++;
                        } else
                            continue;
                        for (i = 0; i < m2.length(); i++) {
                            if (isUp > 0 && isLow > 0 && isDgt > 0) {

                                insc.save(userX);
                                //userRepository.save(userX);
                                model.addAttribute("user", userRepository.findAll());

                                return "rules";
                                // TODO Il faut ajouter le test sur les caractères spéciaux
                            } else {
                                break;
                                //   return "inscription";
                            }
                        }
                    }
                    // TODO Ensuite il faut créer une condition qui validera un mot de passe
                    //  contenant au moins 3 des 4 types de caractères présent dans le mot de passe

                }
            }
        }
        return "inscription";
    }

    @RequestMapping(value = "/Connexion", method = RequestMethod.POST)
    public String connexion(Model model, @RequestParam("name") String n0, @RequestParam("psw") String m2) {

        // algo:
        // l'utilisateur donne son nom et son mot de passe
        // le system doit comparer avec ce qui est présent dans la bdd
        //
        User userY = new User();


        return "#";
    }

}
