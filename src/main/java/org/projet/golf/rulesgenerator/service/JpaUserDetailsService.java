package org.projet.golf.rulesgenerator.service;

import org.projet.golf.rulesgenerator.models.Groups;
import org.projet.golf.rulesgenerator.models.User;
import org.projet.golf.rulesgenerator.models.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;


    @Service
    public class JpaUserDetailsService implements UserDetailsService {


        private UserRepository userRepository;

        private Logger log = LoggerFactory.getLogger(this.getClass());

        @Autowired
        public void setUserRepository(UserRepository userRepository){
            this.userRepository = userRepository;
        }


        @Override
        @Transactional(readOnly = true)
        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

            User user = userRepository.findByName(username);
            log.info("Recherche utilisateur: "+username);
            if(user == null){
                throw new UsernameNotFoundException("Utilisateur introuvable : |"+username+"|");
            }

            Set<GrantedAuthority> authorities = new HashSet<>();
            for(Groups grp: user.getGroups()){
                log.info("{username: "+username+"| grp: "+grp.getRole());
                authorities.add(new SimpleGrantedAuthority(grp.getRole()));
            }

            // BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            // log.info("Test pwd : "+encoder.encode("passe"));

            return new org.springframework.security.core.userdetails.User(
                    user.getName(),
                    user.getPassword(),
                    authorities);
        }

    }


