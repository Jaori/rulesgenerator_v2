package org.projet.golf.rulesgenerator.service;

import org.projet.golf.rulesgenerator.models.GroupRepository;
import org.projet.golf.rulesgenerator.models.User;
import org.projet.golf.rulesgenerator.models.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
public class JpaUserService {
    private UserRepository userRepository;
    private GroupRepository groupRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public void setUserDao(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Autowired
    public void setGroupDao(GroupRepository groupRepository){
        this.groupRepository = groupRepository;
    }


    @Autowired
    public void setbCryptPasswordEncoder(BCryptPasswordEncoder bCryptPasswordEncoder){
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void save(User user){
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setGroups(new HashSet<>(groupRepository.findAll()));
        userRepository.save(user);
    }

    public User findByUserName(String userName){
        return userRepository.findByName(userName);
    }


}
