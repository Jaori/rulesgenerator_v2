package org.projet.golf.rulesgenerator;

import org.junit.jupiter.api.Test;
import org.projet.golf.rulesgenerator.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class RulesgeneratorApplicationTests {
	@Autowired
	private RegleRepository regleRepository;

	@Autowired
	private CategorieRepository categorieRepository;

	@Autowired
	private SousCategorieRepository sousCategorieRepository;

	@Test
	void contextLoads() {
		Categorie cat1 = new Categorie();
		cat1.setTitre("Première catégorie");

		System.out.println("Id catégorie avant enregistrement par JPA");
		System.out.println(cat1.getId());

		categorieRepository.save(cat1);

		System.out.println("Id catégorie après enregistrement par JPA");
		System.out.println(cat1.getId());

		assertThat(sousCategorieRepository.count()).isEqualTo(2);

	}
	@Test
	void contextLoad2() {
		Categorie cat = new Categorie();
		cat.setTitre("Première catégorie");

		assertThat(cat.getId()).isNull();

		categorieRepository.save(cat);

		assertThat(cat.getId()).isNotNull();

		assertThat(cat.getSousCategorieList().size()).isEqualTo(0);

		assertThat(sousCategorieRepository.count()).isEqualTo(0);

		SousCategorie sousCat = new SousCategorie();
		sousCat.setTitre("Hello");
		cat.getSousCategorieList().add(sousCat);

		sousCategorieRepository.save(sousCat);
		categorieRepository.save(cat);

		assertThat(sousCategorieRepository.count()).isEqualTo(3);

		Categorie cat0 = categorieRepository.getOne(cat.getId());
		assertThat(cat0.getSousCategorieList().size()).isEqualTo(1);
	}

	@Test
	void contextLoad3(){
		Categorie cat = new Categorie();
		cat.setTitre("Première catégorie");

		assertThat(cat.getId()).isNull();

		categorieRepository.save(cat);

		assertThat(cat.getId()).isNotNull();

		assertThat(cat.getSousCategorieList().size()).isEqualTo(0);

		assertThat(sousCategorieRepository.count()).isEqualTo(0);

		SousCategorie sousCat = new SousCategorie();
		sousCat.setTitre("Hello");

		cat.getSousCategorieList().add(sousCat);

		sousCategorieRepository.save(sousCat);
		categorieRepository.save(cat);

		assertThat(sousCategorieRepository.count()).isEqualTo(1);

		Categorie catDb = categorieRepository.getOne(cat.getId());
		assertThat(catDb.getSousCategorieList().size()).isEqualTo(1);
	}
	@Test
	//@Transactional
	void createCategorie() {
		Categorie cat = new Categorie();
		cat.setTitre("Première catégorie");
		categorieRepository.save(cat);
		assertThat(cat.getId()).isNotNull();

		assertThat(cat.getSousCategorieList().size()).isEqualTo(0);

		assertThat(sousCategorieRepository.count()).isEqualTo(0);

		SousCategorie sousCat = new SousCategorie();
		sousCat.setTitre("hello");
		cat.getSousCategorieList().add(sousCat);

		sousCategorieRepository.save(sousCat);
		categorieRepository.save(cat);

		assertThat(sousCategorieRepository.count()).isEqualTo(1);
		Categorie cat0 = categorieRepository.getOne(cat.getId());
		assertThat(cat0.getSousCategorieList().size()).isEqualTo(1);
	}

	@Test
	//@Transactional
	void sousCategorie() {

		//Assert.isTrue(sousCategorieRepository.count()==0," pas sous cat");
		assertThat(sousCategorieRepository.count()).isEqualTo(0);
		SousCategorie sousCat = new SousCategorie();
		sousCat.setTitre("Hello");
		Regle regle= new Regle();
		sousCat.getRegles().add(regle);
		regleRepository.save(regle);
		sousCategorieRepository.save(sousCat);

		assertThat(sousCategorieRepository.count()).isEqualTo(1);
		SousCategorie scat0 = sousCategorieRepository.getOne(sousCat.getId());
		assertThat(scat0.getRegles().size()).isEqualTo(1);
		//Assert.isTrue(sousCategorieRepository.count()==1," il doit y avoir 1 scat");
	}

	@Test
	void createRegle(){
	}

}
