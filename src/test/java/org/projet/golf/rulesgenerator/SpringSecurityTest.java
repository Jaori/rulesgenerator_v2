package org.projet.golf.rulesgenerator;

import org.junit.jupiter.api.Test;
import org.projet.golf.rulesgenerator.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class SpringSecurityTest {
    @Autowired
    private RegleRepository regleRepository;

    @Autowired
    private CategorieRepository categorieRepository;

    @Autowired
    private SousCategorieRepository sousCategorieRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void UserIncription(){
        User user1 = new User();
        user1.setMail("admin@test.fr");
        assertThat(groupRepository.count()).isEqualTo(3);
        List<Groups> groups= (List<Groups>) groupRepository.findAll();
        for (Groups gr: groups) {
            System.out.println("id du role ="+gr.getId()+" nom du role "+gr.getName());}
        Groups admin= groupRepository.findById(1L).get();
        assertThat(admin.getName()).isEqualTo("Admin");
        user1.setGroups(new HashSet<>());
        user1.getGroups().add(admin);
        user1.setName("Dolly Prane");
        user1.setPassword("admin");

       userRepository.save(user1);

      //  admin.getUsers().add(user1);
      //  groupRepository.save(admin);
    }

  /*    Test
        Transactional
        ajoututilisateur() {

        JpaUserService JDS =new JpaUserService();
        JDS.setGroupDao(groupRepository);
        JDS.setUserDao(userRepository);
        JDS.setbCryptPasswordEncoder(new BCryptPasswordEncoder());
        Utilisateur user1 = new Utilisateur();
        user1.setMail("admin@test.fr");
        assertThat(roleRepository.count()).isEqualTo(3);
        List<Role> roles= (List<Role>) roleRepository.findAll();
        for (Role r: roles) {
            System.out.println("id du role ="+r.getId()+" nom du role "+r.getName());
        }
        Role admin= roleRepository.findById(1L).get();
        assertThat(admin.getName()).isEqualTo("Admin");

        user1.setName("Dolly Prane");
        user1.setPassword("admin");
        JDS.save(user1);
        admin.getUsers().add(user1);
        roleRepository.save(admin);
        Utilisateur test1=utilisateurRepository.findById(1L).get();
        System.out.println(" insert into PUBLIC.UTILISATEUR (ID, ACTIVE, MAIL, NAME, PASSWORD) VALUES ("+test1.getId()+","+false+","+test1.getMail()+","+test1.getName()+","+test1.getPassword()+");");
    */
    }




